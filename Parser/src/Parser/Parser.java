/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parser;

import java.io.*;

/**
 *
 * @author dean
 */
public class Parser {

    static int lookahead;

    /**
     *  Constructs the parse class to read from the input stream  
     * @throws java.io.IOException
     */
    public Parser() throws IOException {
        lookahead = System.in.read();
    }

    /**
     * Evaluates an expression (+/- only)
     * @throws IOException 
     */
    void expr() throws IOException {
        term();

        while (true) {
            if (lookahead == '+') {
                match('+');
                term();
                System.out.write('+');
            } else if (lookahead == '-') {
                match('-');
                term();
                System.out.write('-');
            } else {
                return;
            }
        }
    }
    
    /**
     *  Looks for a terminal character
     * @throws IOException 
     */
    void term() throws IOException{
        if (Character.isDigit((char)lookahead)) {
            System.out.write((char)lookahead);
            match(lookahead);
        }
        else throw new Error("syntax error");
    }
    
    /**
     *  Checks the current lookahead against parameter, then moves the input
     *  to the next character in the input stream
     * @param t
     * @throws IOException 
     */
    void match(int t) throws IOException
    {
        if (lookahead == t) 
            lookahead = System.in.read();
        else throw new Error("syntax error");
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symbols;

import java.util.*;
import lexer.*;
import inter.*;


/**
 *
 * @author dean
 */
public class Env {
    private Hashtable table;
    protected Env prev;
    
    public Env(Env n) { 
        table = new Hashtable(); 
        prev = n; 
    }
    
    public void put(Token w, Id i) {
        table.put(w, i);
    }
    
    public Id get(Token w) {
        for (Env e = this; e != null; e = e.prev) {
            Id found = (Id)(e.table.get(w));
            
            if (found != null) return found;
        }
        return null;
    }
}

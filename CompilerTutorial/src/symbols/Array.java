/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package symbols;

import lexer.*;

/**
 *
 * @author dean
 */
public class Array extends Type {
    public Type of;
    
    public int size = 1;
    
    public Array(int sz, Type p) {
        super("[]", Tag.INDEX, sz*p.width);
        size = sz;
        of = p;
    }
    
    @Override
    public String toString() { return "[" + size + "]" + of.toString(); }
}

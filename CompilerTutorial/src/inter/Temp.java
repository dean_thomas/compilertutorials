/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import lexer.*;
import symbols.*;

/**
 *
 * @author dean
 */
public class Temp extends Expr {
    static int count = 0;
    
    int number = 0;
    
    public Temp(Type p) {
        super(Word.temp, p);
        
        number = ++count;
    }
    
    @Override
    public String toString() {
        return "t" + number;
    }
}

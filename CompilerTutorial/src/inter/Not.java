/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import lexer.*;
import symbols.*;

/**
 *
 * @author dean
 */
public class Not extends Logical {
    public Not(Token tok, Expr x2) {
        super(tok, x2, x2);
    }
    
    @Override
    public void jumping(int t, int f) {
        expr2.jumping(f, t);
    }
    
    @Override
    public String toString() {
        return op.toString() + " " + expr2.toString();
    }
}

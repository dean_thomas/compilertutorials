/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import lexer.*;
import symbols.*;

/**
 *
 * @author dean
 */
public class Or extends Logical {
    public Or(Token tok, Expr x1, Expr x2) {
        super(tok, x1, x2);
    }
    
    @Override
    public void jumping(int t, int f) {
        int label = t != 0 ? t : newlabel();
        
        expr1.jumping(label, 0);
        expr2.jumping(t, f);
        
        if (t == 0) emitlabel(label);
    }
}

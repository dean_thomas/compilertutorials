/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import symbols.*;

/**
 *
 * @author dean
 */
public class Do extends Stmt {
    Expr expr;
    Stmt stmt;
    
    public Do() { 
        expr = null;
        stmt = null;
    }
    
    public void init(Stmt s, Expr x) {
        expr = x;
        stmt = s;
        
        if (expr.type != Type.Bool) expr.error("boolean required in do");
    }
    
    @Override
    public void gen(int b, int a) {
        after = a;
        
        int label = newlabel();
        
        stmt.gen(b, label);
        emitlabel(label);
        
        expr.jumping(b, 0);
    }
}

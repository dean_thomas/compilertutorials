/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import lexer.*;
import symbols.*;

/**
 *
 * @author dean
 */
public class Unary extends Op {
    public Expr expr;
    
    public Unary(Token tok, Expr x) {
        super(tok, null);
        
        expr = x;
        type = Type.max(Type.Int, expr.type);
        if (type == null) error("type error");
    }
    
    @Override
    public Expr gen() {
        return new Unary(op, expr.reduce());
    }
    
    @Override
    public String toString() {
        return op.toString() + " " + expr.toString();
    }
}

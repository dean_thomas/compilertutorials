/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inter;

import lexer.*;
import symbols.*;

/**
 *
 * @author dean
 */
public class Id extends Expr {
    public int offset;
    
    public Id(Word id, Type p, int b) {
        super(id, p);
        offset = b;
    }
}
